﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV;
using System.Runtime.InteropServices;

namespace WebCamWrapper
{
    public class EmguWebcam
    {
        private Image _webcamTarget;
        private Capture _capture;
        DispatcherTimer _timer;

        private bool captureEnabled;
        private bool captureRequested;
        private string picturePath;

        public EmguWebcam(Image webcamTarget)
        {
            _webcamTarget = webcamTarget;
            _capture = new Capture();
            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(timer_Tick);
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Image<Bgr, Byte> currentFrame = _capture.QueryFrame();

            if (currentFrame != null)
            {
                _webcamTarget.Source = ToBitmapSource(currentFrame);

                if (captureRequested)
                {
                    _capture.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 1920);
                    _capture.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 1080);
                    currentFrame = _capture.QueryFrame();

                    currentFrame.Save(picturePath);
                    captureRequested = false;

                    _capture.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 640);
                    _capture.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 480);
                }
            }

        }

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop
                  .Imaging.CreateBitmapSourceFromHBitmap(
                  ptr,
                  IntPtr.Zero,
                  Int32Rect.Empty,
                  System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }

        public void CaptureImage(string filePath)
        {
            if (!captureEnabled)
                throw new InvalidOperationException("Camera must be started before capturing images!");

            picturePath = filePath;
            captureRequested = true;
        }

        public void StartCapture()
        {
            _timer.Start();
            captureEnabled = true;
        }

        public void StopCapture()
        {
            _timer.Stop();
            captureEnabled = false;
        }
    }
}
