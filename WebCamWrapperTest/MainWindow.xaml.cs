﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebCamWrapper;

namespace WebCamWrapperTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private EmguWebcam webcam;
        private int counter;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            webcam = new EmguWebcam(WebcamTarget);
        }

        private void CaptureButton_Click(object sender, RoutedEventArgs e)
        {
            webcam.CaptureImage(string.Format(@"C:\WebcamCaptureTest{0}.jpg", counter));
            counter++;
        }

        private void StartButton_OnClick(object sender, RoutedEventArgs e)
        {
            webcam.StartCapture();
        }

        private void StopButton_OnClick(object sender, RoutedEventArgs e)
        {
            webcam.StopCapture();
        }
    }
}
